#include "mainwindow.h"
#include "ui_mainwindow.h"

#include <QDebug>

#pragma comment(lib,"setupapi.lib")

MainWindow::MainWindow(QWidget *parent)
    : QMainWindow(parent)
    , ui(new Ui::MainWindow)
{
    ui->setupUi(this);
}

MainWindow::~MainWindow()
{
    delete ui;
}


void MainWindow::on_pushButton_clicked()
{

    string csStr = GetUsbInfo();

//    qDebug() << QString::fromStdString( csStr);
}

//{}
DEFINE_GUID (UsbClassGuid, 0x36fc9e60, 0xc465, 0x11cf, 0x80, 0x56, 0x44, 0x45, 0x53, 0x54, 0x00, 0x00);

//DEFINE_GUID (UsbClassGuid, 0xa5dcbf10L, 0x6530, 0x11d2, 0x90, 0x1f, 0x00, 0xc0, 0x4f, 0xb9, 0x51, 0xed);

//获取USB设备VID和PID
string MainWindow::GetUsbInfo()
{

     HDEVINFO hDevInfo;

     SP_DEVICE_INTERFACE_DATA spDevData;

     PSP_DEVICE_INTERFACE_DETAIL_DATA pDetail;

     BOOL bRes = TRUE;

     int nCount = 0;
     string csResult;
     hDevInfo = ::SetupDiGetClassDevs((LPGUID) &UsbClassGuid,NULL,NULL,DIGCF_PRESENT | DIGCF_INTERFACEDEVICE);

     SP_DEVINFO_DATA deviceInfoData = {sizeof(deviceInfoData)};

     if (hDevInfo != INVALID_HANDLE_VALUE)
     {
         for (int i = 0; SetupDiEnumDeviceInfo(hDevInfo, i, &deviceInfoData); i++)
         {
             DWORD data;
             wchar_t wcbuffer[2046] = {0};
             DWORD buffersize = 2046;
             DWORD req_bufsize = 0;

             if (!SetupDiGetDeviceRegistryProperty(hDevInfo, &deviceInfoData, SPDRP_LOCATION_INFORMATION, &data,
                                                   (LPBYTE)wcbuffer, buffersize, &req_bufsize))
             {
                 qDebug() << "continue";
             }

             QString localInfo = QString::fromWCharArray(wcbuffer);
             qDebug() << i << " location: " << localInfo;

             pDetail = (PSP_DEVICE_INTERFACE_DETAIL_DATA)::GlobalAlloc(LMEM_ZEROINIT,1024);

             pDetail->cbSize = sizeof(SP_DEVICE_INTERFACE_DETAIL_DATA);

             while (bRes)
             {
                 spDevData.cbSize = sizeof(SP_DEVICE_INTERFACE_DATA);
                 bRes = ::SetupDiEnumDeviceInterfaces(hDevInfo, &deviceInfoData,(LPGUID)&UsbClassGuid,nCount,&spDevData);

                 if (bRes)
                 {
                     bRes = ::SetupDiGetInterfaceDeviceDetail(hDevInfo,&spDevData,pDetail,1024,NULL,&deviceInfoData);

                     if (bRes)
                     {
                         wstring wszStr = pDetail->DevicePath;
                         string szStr(wszStr.begin(), wszStr.end());// = pDetail->DevicePath;
                         csResult += szStr +"\n";
                         nCount ++;
                     }
                 }

             }
             ::GlobalFree(pDetail);


//             DWORD dwCount = 128;
//             DEVPROPKEY devicePropertyKey[256] = {0};
//             WCHAR PropertyBuffer[1024] = {0};

//             if (!SetupDiGetDevicePropertyKeys(hDevInfo, &deviceInfoData, devicePropertyKey, dwCount, 0,0))
//                 continue;

//             DEVPROPTYPE PropertyType = 0;
//             SetupDiGetDevicePropertyW(hDevInfo, &deviceInfoData, &devicePropertyKey[0], &PropertyType, reinterpret_cast<PBYTE>(PropertyBuffer), sizeof(PropertyBuffer), NULL, 0);//)

             qDebug() << i << " sn " << QString::fromStdString( csResult);

//             QString s = QString::fromWCharArray(csResult);
//             qDebug() << "afdadf " << s;


         }
         ::SetupDiDestroyDeviceInfoList(hDevInfo);
     }

     return csResult;
}


