#include "mainwindow.h"
#include "ui_mainwindow.h"
#include "filehashtool.h"

#include <QFileDialog>
#include <QFileInfo>
#include <QDebug>

MainWindow::MainWindow(QWidget *parent)
    : QMainWindow(parent)
    , ui(new Ui::MainWindow)
{
    ui->setupUi(this);
}

MainWindow::~MainWindow()
{
    delete ui;
}


void MainWindow::on_pushButton_clicked()
{
//    QFileInfo fileInfo;
//    QString file, fileName, fileSuffix, filePath;
//    file = QFileDialog::getOpenFileName();
//    fileInfo = QFileInfo(file);
//    //文件名
//    fileName = fileInfo.fileName();
//    //文件后缀
//    fileSuffix = fileInfo.suffix();
//    //绝对路径
//    filePath = fileInfo.absolutePath();
//    qDebug() << fileName << '\n'
//                << fileSuffix<< '\n'
//                << filePath<< '\n';

    QFileDialog fileD = QFileDialog();
    //设置可以打开任何文件
    fileD.setFileMode(QFileDialog::AnyFile);
    //文件过滤
    fileD.setFilter(QDir::Files);

     if (fileD.exec())
     {
         QString fileName = fileD.selectedFiles()[0];
//         QFileInfo fileinfo = QFileInfo(fileName);
//         QString name = fileinfo.baseName();

         QString hashStr = FileHashTool::hashOneFile(fileName); //("D:/SogouInput/Components/crash/errorlog.txt"); //filePath + fileName);

         qDebug() << "file hash: " + hashStr;
     }
}


void MainWindow::on_pushButton_2_clicked()
{
    QString  filename_src = QFileDialog::getExistingDirectory();
    qDebug() << "file father path: " + filename_src;

    QString hashStr = FileHashTool::hashSomeFile(filename_src, "errorlog", "txt");

    qDebug() << "file hash: " + hashStr;
}

