#ifndef FILEHASHTOOL_H
#define FILEHASHTOOL_H

#include <QObject>

class FileHashTool
{
public:
    FileHashTool();

    //! filePath as absolute file path
    static QString hashOneFile(QString filePath);

    static QString hashSomeFile(QString fatherPath, QString filePrefix, QString fileType);

};

#endif // FILEHASHTOOL_H
