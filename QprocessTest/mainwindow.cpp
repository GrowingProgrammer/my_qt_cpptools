#include "mainwindow.h"
#include "ui_mainwindow.h"

#include <QProcess>
#include <QDebug>
#include <QTextCodec>

MainWindow::MainWindow(QWidget *parent)
    : QMainWindow(parent)
    , ui(new Ui::MainWindow)
{
    ui->setupUi(this);
}

MainWindow::~MainWindow()
{
    delete ui;
}


void MainWindow::on_pushButton_clicked()
{
//    QProcess p(0);
//    p.startDetached("cmd", QStringList() <<"/c"<<"dir");//找网络ip的指令（netstat -aon|findstr 218.5.241.13:211）
//    p.waitForStarted();
//    p.waitForFinished();
//    qDebug()<<QString::fromLocal8Bit(p.readAllStandardOutput());
//    return;

    QString appRootPath = qApp->applicationDirPath();

    qp = new QProcess(this);
    QStringList l;
    l <<"";// "timeout" << "20";

    qp->setStandardOutputFile("log_1.log");
    qp->setWorkingDirectory(appRootPath + "/BG/");

    qp->startDetached("main.bat", l);
    qp->waitForFinished();

    connect(qp, &QProcess::readyReadStandardOutput, this, &MainWindow::slot_readOutput);
}

void MainWindow::slot_readOutput()
{
    QTextCodec* gbkCodec = QTextCodec::codecForName("GBK");
    QString result = gbkCodec->toUnicode(qp->readAll());
    qDebug()<<result;
}

void MainWindow::slot_finished()
{
    qDebug() << "finished";
}

