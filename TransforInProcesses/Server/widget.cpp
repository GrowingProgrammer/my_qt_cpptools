#include "widget.h"
#include "ui_widget.h"

Widget::Widget(QWidget *parent)
    : QWidget(parent)
    , ui(new Ui::Widget)
{
    ui->setupUi(this);
    tcpserver=new QTcpServer(this);
    tcpsocket=new QTcpSocket(this);

    connect(tcpserver,SIGNAL(newConnection()),this,SLOT(newConnection_Slot()));
}
void Widget::newConnection_Slot()
{
    tcpsocket=tcpserver->nextPendingConnection(); //获取已经连接的客户端套接字
    connect(tcpsocket,SIGNAL(readyRead()),this,SLOT(readyRead_Slot()));
}
void Widget::readyRead_Slot()
{
     QString buf;
     buf=tcpsocket->readAll();
     ui->receivewd->appendPlainText(buf);
}
Widget::~Widget()
{
    delete ui;
}
void Widget::on_connectbt_clicked()//打开服务器
{
    tcpserver->listen(QHostAddress::Any,ui->portnum->text().toUInt());  //监听端口号
}
void Widget::on_disconnectbt_clicked()//关闭服务器
{
    tcpserver->close();
}
void Widget::on_sendbt_clicked()//发送
{
    tcpsocket->write(ui->sendwd->text().toLocal8Bit().data());
}

