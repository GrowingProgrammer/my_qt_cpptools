#ifndef WIDGET_H
#define WIDGET_H

#include <QWidget>
#include <QTcpServer>
#include <QTcpSocket>
#include <QString>
QT_BEGIN_NAMESPACE
namespace Ui { class Widget; }
QT_END_NAMESPACE
class Widget : public QWidget
{
    Q_OBJECT
public:
    Widget(QWidget *parent = nullptr);
    ~Widget();
    QTcpServer *tcpserver;
    QTcpSocket *tcpsocket;
private slots:
    void on_connectbt_clicked();
    void newConnection_Slot();
    void readyRead_Slot();
    void on_disconnectbt_clicked();
    void on_sendbt_clicked();
private:
    Ui::Widget *ui;
};
#endif // WIDGET_H
