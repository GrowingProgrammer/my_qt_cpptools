#include <QCoreApplication>

int main1(int argc, char *argv[])
{
    QCoreApplication a(argc, argv);

    return a.exec();
}

#include <iostream>
#include <Windows.h>

using namespace std;

#define BUF_SIZE 4096
HANDLE H_Mutex = NULL;
HANDLE H_Event = NULL;

int main12(int argc,char ** argv)
{

    //步骤1：创建共享文件句柄
    HANDLE shared_file = CreateFileMapping(
                INVALID_HANDLE_VALUE,//物理文件句柄
                NULL,  //默认安全级别
                PAGE_READWRITE,      //PAGE_READWRITE表示可读可写，PAGE_READONLY表示只读，PAGE_WRITECOPY表示只写
                0,  //高位文件大小
                BUF_SIZE,  //低位文件大小
                L"ShareMemory"  //共享内存名称
                );

    if (shared_file == NULL)
    {
        cout<<"Could not create file mapping object..."<<endl;
        return 1;
    }

    //步骤2：映射缓存区视图，得到指向共享内存的指针
    LPVOID lpBUF = MapViewOfFile(
                shared_file, //已创建的文件映射对象句柄
                FILE_MAP_ALL_ACCESS,//访问模式:可读写
                0, //文件偏移的高32位
                0, //文件偏移的低32位
                BUF_SIZE //映射视图的大小
                );

    if (lpBUF == NULL)
    {
        cout << "Could not create file mapping object..." << endl;
        CloseHandle(shared_file);
        return 1;
    }

    H_Mutex = CreateMutex(NULL, FALSE, L"sm_mutex");//创建一个互斥器
    H_Event = CreateEvent(NULL,//表示安全控制，一般为NULL
                          FALSE,//确定事件是手动置位还是自动置位，传入TRUE表示手动置位，传入FALSE表示自动置位。如果为自动置位，则对该事件调用WaitForSingleObject()后会自动调用ResetEvent()使事件变成未触发状态。
                          FALSE,//表示事件的初始状态，传入TRUE表示已触发
                          L"sm_event"//表示事件的名称，传入NULL表示匿名事件
                          );



    //步骤3：操作共享内存
    char Buffer[97];
    while (1)
    {
        cout << "A proccess:Please input the content to the process B" << endl;
        cin.getline(Buffer,97);
        cout << "Buffer: " <<Buffer<< endl;
        WaitForSingleObject(H_Mutex, INFINITE); //使用互斥体加锁   获得互斥器的拥有权
        memcpy(lpBUF, Buffer, strlen(Buffer)+1);
        ReleaseMutex(H_Mutex); //放锁
        SetEvent(H_Event);//激活等待的进程
    }

    CloseHandle(H_Mutex);
    CloseHandle(H_Event);
    //步骤4：解除映射和关闭句柄
    UnmapViewOfFile(lpBUF);
    CloseHandle(shared_file);
    return 0;
}


#include <Windows.h>
#include <iostream>

using namespace std;

#define BUF_SIZE 4096
HANDLE h_Mypipe = NULL;

//步骤1：定义管道名，点表示当前主机，pipe表示管道
#define MY_NAMED_PIPE L"\\\\.\\pipe\\Named_Pipe"

int main123(int argc, char ** argv)
{
    //步骤2：创建命名管道
    h_Mypipe = CreateNamedPipe(
                MY_NAMED_PIPE, //为命名管道创建名称
                PIPE_ACCESS_DUPLEX, //管道访问方式：PIPE_ACCESS_DUPLEX指双向模式
                PIPE_TYPE_MESSAGE | //命名管道句柄的写入方式：以数据块的方式写入管道
                PIPE_READMODE_MESSAGE | //命名管道句柄的读取方式：以数据块的方式从管道读取
                PIPE_WAIT, //命名管道句柄的等待方式：阻塞方式
                PIPE_UNLIMITED_INSTANCES, //管道所能创建的最大实例个数：1~255，
                0, //管道的输出缓冲区容量，0表示默认大小
                0, //管道的输入缓冲区容量，0表示默认大小
                1000, //管道的默认等待超时，单位毫秒
                NULL); //管道的安全性，NULL表示windows提供的默认安全

    //INVALID_HANDLE_VALUE是CreateNamedPipe返回值，表示创建失败
    if (h_Mypipe == INVALID_HANDLE_VALUE)
    {
        cout << "Create Named_Pipe Failed..." << endl;
        return 1;
    }

    //步骤3：等待客户端的连接
    if (!ConnectNamedPipe(h_Mypipe, NULL))
    {
        cout << "Connect Failed..." << endl;
        return 1;
    }
    else
        cout << "Connect Successed..." << endl;

    DWORD wLen = 0;
    DWORD rLen = 0;
    char szBuffer[BUF_SIZE] = { 0 };

    //步骤4：读写管道
    while (1)
    {
        //向客户端发送数据
        cin.getline(szBuffer, BUF_SIZE);
        cout << "1服务器端发送数据：" << szBuffer<< endl;
        if (!WriteFile(h_Mypipe, szBuffer, strlen(szBuffer) + 1, &wLen, NULL))
            cout << "Write Failed..." << endl;
        else
            cout<<"2服务器端发送成功：共"<< wLen<<"byte"<<endl;

        //清除缓冲区
        //memset(szBuffer, 0, BUF_SIZE);

        //读取客户端数据
        if (!ReadFile(h_Mypipe, szBuffer, BUF_SIZE, &rLen, NULL))
            cout << "Read Failed..." << endl;
        else
            cout << "3服务器接收客户端数据：" << szBuffer << ", 共" << rLen << "byte" << endl;
    }

    cout << "closed";
    //步骤5：关闭管道
    DisconnectNamedPipe(h_Mypipe);
    CloseHandle(h_Mypipe);
    return 0;
}




#include<iostream>
#include<winsock.h>

#pragma comment(lib,"ws2_32.lib")
using namespace std;
void initialization();

int main(int argc, char ** argv)
{
    //定义长度变量
    int send_len = 0;
    int recv_len = 0;
    //定义发送缓冲区和接受缓冲区
    char send_buf[100];
    char recv_buf[100];
    //定义服务端套接字，接受请求套接字
    SOCKET s_server;
    //服务端地址客户端地址
    SOCKADDR_IN server_addr;
    initialization();
    //填充服务端信息
    server_addr.sin_family = AF_INET;
    server_addr.sin_addr.S_un.S_addr = inet_addr("127.0.0.1");
    server_addr.sin_port = htons(4530);
    //创建套接字
    s_server = socket(AF_INET, SOCK_STREAM, 0);
    if (connect(s_server, (SOCKADDR*)& server_addr, sizeof(SOCKADDR)) == SOCKET_ERROR) {
        cout << "connect server failed" << endl;
        WSACleanup();
    }
    else {
        cout << "connect server successfully" << endl;
    }

    //发送,接收数据
    while (1)
    {
        cout << "please input data:" << endl;
//        cin >> send_buf;
        cin.getline(send_buf, BUF_SIZE);
//        cout << "send value:" << send_buf << endl;
        //数据转换转成unicode编码
        char buffer[1024] = "\0";
        strcpy(buffer, send_buf);
//        cout << "send value:" << buffer << endl;
        int nSize = strlen(buffer);

        WCHAR bufU[1024] = L"\0";
        int bufUSize = MultiByteToWideChar(CP_ACP, 0, buffer, -1, bufU, 1024);
        size_t tempsize = wcslen(bufU);

        send_len = send(s_server, (char *)bufU, bufUSize*2, 0);
        if (send_len < 0) {
            cout << "failed to send" << endl;
            break;
        }
        recv_len = recv(s_server, recv_buf, 100, 0);
        if (recv_len < 0) {
            cout << "read data failed" << endl;
            break;
        }
        else {
            cout << "server response:" << recv_buf << endl;
        }

    }
    //关闭套接字
    closesocket(s_server);
    //释放DLL资源
    WSACleanup();
    return 0;
}

void initialization()
{
    //初始化套接字库
    WORD w_req = MAKEWORD(2, 2);//版本号
    WSADATA wsadata;
    int err;
    err = WSAStartup(w_req, &wsadata);
    if (err != 0) {
        cout << "init socket failed" << endl;
    }
    else {
        cout << "init socket successfully" << endl;
    }
    //检测版本号
    if (LOBYTE(wsadata.wVersion) != 2 || HIBYTE(wsadata.wHighVersion) != 2) {
        cout << "socket version error" << endl;
        WSACleanup();
    }
    else {
        cout << "socket version correct" << endl;
    }
    //填充服务端地址信息

}

