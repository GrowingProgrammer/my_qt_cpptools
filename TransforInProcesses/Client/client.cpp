#include "client.h"
#include "ui_client.h"
#include "QString"
#include "stdio.h"

client::client(QWidget *parent)
    : QMainWindow(parent)
    , ui(new Ui::client)
{
    ui->setupUi(this);
    tcpsocket=new QTcpSocket(this);
}

client::~client()
{
    delete ui;
}

void client::connected_SLOT()
{
    connect(tcpsocket,SIGNAL(readyRead()),this,SLOT(readyRead_Slot()));//将信号连接到槽，书写比较明确
}
void client::readyRead_Slot()//定义接收信号的槽
{
    QString buf;
    buf=tcpsocket->readAll();
    ui->receivewd->appendPlainText(buf);//接收由tcp发送过来的信息

}
void client::on_openclient_clicked()
{
      tcpsocket->connectToHost(ui->ipnum->text(),ui->portnum->text().toUInt());//转为无符号，连接服务器端口
      connect(tcpsocket,SIGNAL(connected()),this,SLOT(connected_SLOT()));
      printf("打开客户端 ");
}

void client::on_closeclient_clicked()
{
    tcpsocket->close();
    printf("关闭客户端 ");

}

void client::on_sent_clicked()
{
    tcpsocket->write(ui->sendwd->text().toLocal8Bit().data());
}

