#ifndef CLIENT_H
#define CLIENT_H

#include <QMainWindow>
#include <QTcpSocket>//服务端只用socket

QT_BEGIN_NAMESPACE
namespace Ui { class client; }
QT_END_NAMESPACE

class client : public QMainWindow
{
    Q_OBJECT

public:
    client(QWidget *parent = nullptr);
    ~client();
    QTcpSocket *tcpsocket;
private slots:
    void on_openclient_clicked();
    void connected_SLOT();
    void readyRead_Slot();
    void on_closeclient_clicked();

    void on_sent_clicked();

private:
    Ui::client *ui;
};
#endif // CLIENT_H

