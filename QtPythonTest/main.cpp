#include <Python.h>
#include "mainwindow.h"

#include <QApplication>
#include <QDebug>

int main(int argc, char *argv[])
{
    Py_SetPythonHome(L"C:\\Program Files\\WindowsApps\\PythonSoftwareFoundation.Python.3.7_3.7.2544.0_x64__qbz5n2kfra8p0\\");
    Py_Initialize();
    if(!Py_IsInitialized())
    {
        return -1;
    }
    PyObject* pModule = PyImport_ImportModule("hi");
    if(!pModule)
    {
        qDebug() <<"open failure";
        return -1;
    }
    PyObject* pFunhello = PyObject_GetAttrString(pModule,"hello");
    if(!pFunhello)
    {
        qDebug() <<"get function hello failed";
        return -1;
    }
    PyObject_CallFunction(pFunhello, NULL);
    Py_Finalize();

    QApplication a(argc, argv);
    MainWindow w;
    w.show();
    return a.exec();
}

